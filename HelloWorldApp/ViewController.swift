//
//  ViewController.swift
//  HelloWorldApp
//
//  Created by Maksim Isaev on 17.02.2020.
//  Copyright © 2020 Maksim Isaev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var helloLabel: UILabel!
    @IBOutlet var startButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        helloLabel.isHidden = true
        startButton.layer.cornerRadius = 10
    }

    @IBAction func startButtinPress() {
        if helloLabel.isHidden {
            helloLabel.isHidden = false
            startButton.setTitle("Clear Text", for: .normal)
        } else {
            helloLabel.isHidden = true
            startButton.setTitle("Show Text", for: .normal)
        }
    }
}

